# AW[K]ESOME

Inspired by https://github.com/mikepea/awk_tawk

Presentation of the programming language [AWK](https://en.wikipedia.org/wiki/AWK).
Specifically the [GNU AWK](https://www.gnu.org/software/gawk/) version: `gawk`.

## Install

```bash
$ git clone --recurse-submodules [REPO_URL]
$ cd awkesome/
$ docker build -t awkesome .
```

## Run

```bash
$ docker run -it --rm -v "$PWD":/app awkesome:latest bash
$ SS=1 awk -f ./present.awk slides.txt # SS optionally gives the starting slide number
```
