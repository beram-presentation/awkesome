# AWK Web Server

Small web server writing with GAWK that serve a file on `8080` port.

## How to use

```bash
$ ./awk-web-server index.html
```

Visit http://localhost:8080 using curl, a browser etc..
