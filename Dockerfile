FROM debian:buster-slim

RUN apt-get update --yes && apt-get install gawk curl lynx htop --yes

WORKDIR /app
